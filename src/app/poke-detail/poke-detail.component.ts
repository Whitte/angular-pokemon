import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {PokemonService} from "../pokemon/pokemon.service";
import {Pokemon} from "../pokemon/pokemon.model";

@Component({
  selector: 'poke-detail',
  templateUrl: './poke-detail.component.html'
})
export class PokeDetailComponent implements OnInit{
  router: ActivatedRoute;
  pokeService: PokemonService;
  pokemon: Pokemon;

  constructor(router: ActivatedRoute, pokeService: PokemonService){
    this.router=router;
    this.pokeService=pokeService;
  }

  ngOnInit(): void {
    let name=this.router.snapshot.params['name'];
    this.pokeService.getByName(name,(pokemon)=>{
      this.pokemon=pokemon;
    });
  }

  loadData(pokemon: Pokemon){
    this.pokemon=pokemon;
  }

  getError(error){
    // this.pokemon=pokemon;
  }

}
