import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpModule} from "@angular/http";
import {PokeListComponent} from "./poke-list/poke-list.component";
import {PokeCardComponent} from "./poke-card/poke-card-component";
import {PokemonService} from "./pokemon/pokemon.service";
import {PokeDetailComponent} from "./poke-detail/poke-detail.component";
import {NavbarComponent} from "./navbar/navbar.component";
import {HomeComponent} from "./home/home.component";
import {DetailComponent} from "./detail/detail.component";
import {SearchbarComponent} from "./searchbar/SearchbarComponent";
import {appRouting} from "./app.routing";
import {PagerComponent} from "./pager/pager.component";

@NgModule({
  declarations: [
    AppComponent,
    PokeListComponent,
    PokeCardComponent,
    PokeDetailComponent,
    NavbarComponent,
    SearchbarComponent,
    HomeComponent,
    DetailComponent,
    PagerComponent
  ],
  imports: [
    BrowserModule, HttpModule, appRouting
  ],
  providers: [PokemonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
