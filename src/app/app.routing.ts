import {RouterModule, Routes} from '@angular/router';
import { HomeComponent} from './home/home.component';
import {PokeListComponent} from "./poke-list/poke-list.component";
import {DetailComponent} from "./detail/detail.component";


const appRoutes: Routes = [
  {
    path: '', component: HomeComponent
  },
  {
    path: 'page/:page', component: HomeComponent
  }
  ,
  {
    path: ':name', component: DetailComponent
  }
];

export const  appRouting = RouterModule.forRoot(appRoutes);
