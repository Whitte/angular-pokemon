import {Component, Input} from "@angular/core";
import {Pokemon} from "../pokemon/pokemon.model";

@Component({
  selector: 'poke-card',
  templateUrl: './poke-card.component.html',
  styles: [`
    img{
      width: 251px;
      height: 242px;
    }
    
    p{
      border: 2px solid #1B3569;
    }
    
    .name-border{
      text-shadow: -2px 0 black, 0 2px black, 2px 0 black, 0 -2px black;
    }
  `]
})
export class PokeCardComponent{
  @Input() pokemon: Pokemon;
}

