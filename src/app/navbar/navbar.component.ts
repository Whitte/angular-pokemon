import {Component, Input} from "@angular/core";

@Component({
  selector: 'app-navbar',
  template: `
    <nav class="navbar">
      <div class="container">
        <img class="img-responsive" src="assets/img/logo-pokemon.png">
      </div>
    </nav>
  `
})
export class NavbarComponent{
}
