import {Component, Input, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html'
})
export class PagerComponent implements OnInit{
  quantity: number;
  _low: number;
  _high: number;
  numbers: number[];
  router: ActivatedRoute;
  page: number;
  @Input() url: string;

  constructor(router: ActivatedRoute){
    this.router=router;
    this.quantity=5;
    this.numbers= [];
  }

  ngOnInit(): void {
    this.router.params.subscribe(params => {
      this.page = params['page'];
      if(this.page==null){
        this.page=1;
      }
    });
    this.low=this.page;
    this.high=this.page;
    this.fillNumbers(this.low);
  }

  fillNumbers(start: number){
    this.numbers=Array.from(new Array(this.quantity),(val,index)=>(index+start));
  }

  getPagerNumber(page:number){
    return Math.ceil((page/this.quantity));
  }

  getPrevPages(low:number){
    this.low=low-1;
    this.high=low-1;
    this.fillNumbers(this.low);
  }

  getNextPages(high:number){
    this.fillNumbers(high+1)
    this.low=high+1;
    this.high=high+1;
  }

  set low(page: number){
    this._low=(this.getPagerNumber(page)-1)*this.quantity+1;
  }
  get low(){return this._low;}

  set high(page: number){
    this._high=this.getPagerNumber(page)*this.quantity;
  }
  get high(){return this._high;}

}
