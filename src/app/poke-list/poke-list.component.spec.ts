import {PokeListComponent} from "./poke-list.component";
import {async, ComponentFixture} from "@angular/core/testing";
import {PokemonService} from "../pokemon/pokemon.service";
import {TestBed} from "@angular/core/testing";
import {Observable} from "rxjs/Observable";
import {HttpModule} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {RouterTestingModule} from "@angular/router/testing";
import {Pokemon} from "../pokemon/pokemon.model";

describe('Component: PokeListComponent', ()=>{
  let fixture: ComponentFixture<PokeListComponent>;
  let component: PokeListComponent;
  let service: PokemonService;

  beforeEach(()=>
  TestBed.configureTestingModule({
    schemas: [NO_ERRORS_SCHEMA],
    imports: [HttpModule, RouterTestingModule],
    providers: [PokemonService,
      {
        provide: ActivatedRoute,
        useValue: {
          params: Observable.of({page: 1})
        }
      }
    ],
    declarations: [PokeListComponent]
  }
  ));

  beforeEach(()=>{
    fixture = TestBed.createComponent(PokeListComponent);
    service = fixture.debugElement.injector.get(PokemonService);
    component=fixture.componentInstance;
  });

  xit('should get the page parameter',()=>{
    fixture.detectChanges();
    expect(component.router.snapshot.params['page']).toBe(1);
  });


  it('should load 3 pokemons into the list',async(()=>{
    spyOn(service,'getPokemonsByPage').and.returnValue(
      Observable.of([new Pokemon('test 1'),new Pokemon('test 2'),new Pokemon('test 3')])
    );
    fixture.detectChanges();
    expect(component.pokemons.length).toEqual(3);
  }));



});

