import {Component} from "@angular/core";

@Component({
  selector: 'app-home',
  template: `
    <div>
      <app-navbar></app-navbar>
      <app-searchbar></app-searchbar>
      <poke-list></poke-list>
      <!--<router-outlet></router-outlet>-->
      <app-pager [url]="'/page/'"></app-pager>
    </div>
 `
})
export class HomeComponent{}
