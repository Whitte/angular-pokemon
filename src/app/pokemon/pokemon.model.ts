
export class Pokemon {
  name: string;
  _image: string;
  description: string;
  height: number;
  category: string;
  weight: number;
  ability: string;
  gender: string;
  weakness: string;


  attack: number;
  defense: number;
  s_attack: number;
  s_defense: number;
  speed: number;



  constructor(name: string, id?: string, description?: string, height?: number, category?: string, weight?: number,
              ability?: string, gender?: string, weakness?: string, attack?: number, defense?: number, spec_att?: number,
              spec_def?: number, speed?: number) {
    this.name = name;
    this._image = 'https://raw.githubusercontent.com/PokeAPI/pokeapi/master/data/v2/sprites/pokemon/'+id+'.png';
    // this._image = 'http://pokeapi.co/media/sprites/pokemon/'+id+'.png';
    this.description = description;
    this.height = height;
    this.category = category;
    this.weight = weight;
    this.ability = ability;
    this.gender = gender;
    this.weakness = weakness;
    this.attack = attack;
    this.defense = defense;
    this.s_attack = spec_att;
    this.s_defense = spec_def;
    this.speed = speed;
  }

  set image(id: string){
    this._image = 'https://raw.githubusercontent.com/PokeAPI/pokeapi/master/data/v2/sprites/pokemon/'+id+'.png';
    // this._image = 'http://pokeapi.co/media/sprites/pokemon/'+id+'.png';
  }

  get image(){
    return this._image;
  }

}
