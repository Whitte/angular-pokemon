
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {DebugElement} from "@angular/core";
import {Pokemon} from "../pokemon/pokemon.model";
import {By} from "@angular/platform-browser";
import {PokeCardComponent} from "./poke-card-component";
import {RouterTestingModule} from "@angular/router/testing";

describe("Component: PokeCardComponent", ()=>{
  let fixture: ComponentFixture<PokeCardComponent>;
  let de: DebugElement;
  let component: PokeCardComponent;

  beforeEach(()=>TestBed.configureTestingModule({
    declarations: [PokeCardComponent],
    imports: [RouterTestingModule]
  }));

  beforeEach(()=>{
    fixture = TestBed.createComponent(PokeCardComponent);
    component= fixture.componentInstance;
    de = fixture.debugElement;
  });

  it("should render the name and image",()=>{
    component.pokemon = new Pokemon("test","1");
    fixture.detectChanges();
    const el = de.query(By.css(".text-capitalize")).nativeElement;
    expect(el.innerText).toBe("test");
    expect(de.query(By.css("img")).nativeElement.src).toBe("https://raw.githubusercontent.com/PokeAPI/pokeapi/master/data/v2/sprites/pokemon/1.png");
  });
});
