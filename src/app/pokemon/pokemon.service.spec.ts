
import {inject, TestBed, async,tick} from "@angular/core/testing";
import {HttpModule} from "@angular/http";
import {PokemonService} from "./pokemon.service";
import {Pokemon} from "./pokemon.model";

xdescribe('Service: PokemonService', () => {
  let testService: PokemonService;

  beforeEach(()=>
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [PokemonService]
    }));

  beforeEach(inject([PokemonService], s=>{
    testService=s;
  }));

  it('should load 20 pokemons from the API', async(() => {
    testService.getPokemonsByPage(1,20).subscribe(pokemons =>{
      expect(testService.pokemons).toBeDefined();
      expect(testService.pokemons.length).toEqual(20);
    });
  }));

  it('should load 20 pokemons even if there are more already loaded', async(() => {
    testService.getPokemonsByPage(1,20).subscribe(()=>{
      testService.getPokemonsByPage(1,20).subscribe((pokemons)=>{
        expect(pokemons.length).toEqual(20);
        expect(testService.pokemons.length).toEqual(20);
      });
    });


  }));

  it('should return the pokemon', ()=>{
    testService.pokemons = [[new Pokemon('test')]];
    let result: Pokemon=testService.findByName('test');
    expect(result.name).toEqual('test');
  });

  it('should call the findPokebyName, getStats and getBasicDataFuncions', () => {
    testService.pokemons = [[new Pokemon('bulbasaur')]];
    spyOn(testService,'getByName').and.callThrough();
    spyOn(testService,'findByName');
    spyOn(testService,'getBasicData').and.callThrough();;
    spyOn(testService,'getStats').and.callThrough();;
    let pokeTest:Pokemon;
    testService.getByName('bulbasaur',(pokemon: Pokemon)=>{
      pokeTest=pokemon;
      expect(pokeTest.category).toBe('Semilla');
    });
    expect(testService.findByName).toHaveBeenCalled();
    expect(testService.getBasicData).toHaveBeenCalled();
    // expect(testService.getStats).toHaveBeenCalled();
  });

  it('should show error when not found', () => {
    testService.pokemons = [[new Pokemon('test')]];

    testService.getByName('bulbasaur',(pokemon: Pokemon)=>{},
      (error)=>{expect(error).toBe('Not found')});

  });

  it('should set the basic data', async(() =>{
    let testPokemon: Pokemon = new Pokemon('bulbasaur');
    testService.getBasicData(testPokemon).subscribe(()=>{
      expect(testPokemon.description).toBeDefined();
      expect(testPokemon.category).toBeDefined();
    });

  }));

  it('should set the stats', async(() =>{
    let testPokemon: Pokemon = new Pokemon('bulbasaur');
    testService.getStats(testPokemon).subscribe(pokemon=>{
      expect(pokemon.attack).toBeDefined();
      expect(pokemon.defense).toBeDefined();
      expect(pokemon.s_attack).toBeDefined();
      expect(pokemon.s_defense).toBeDefined();
      expect(pokemon.speed).toBeDefined();
    });
  }));

});
