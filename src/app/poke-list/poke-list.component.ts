import {Component, OnInit} from "@angular/core";
import {Pokemon} from "../pokemon/pokemon.model";
import {PokemonService} from "../pokemon/pokemon.service";
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'poke-list',
  templateUrl: './poke-list.component.html'
})
export class PokeListComponent implements OnInit{
  pokemons: Pokemon[];
  pokemonService: PokemonService;
  router: ActivatedRoute;

  constructor(pokemonService: PokemonService, router: ActivatedRoute){
    this.pokemonService = pokemonService;
    this.router=router;
  }

  ngOnInit(): void {
    this.router.params.subscribe(
      params => {
        let page = params['page'];
        if(page==null){page=1;}
        this.pokemonService.getPokemonsByPage(page,20)
          .subscribe(
            (pokemons: Pokemon[]) => {
              this.pokemons = pokemons;
            }
          );
      }
    );

  }
}
