
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {PagerComponent} from "./pager.component";
import {RouterTestingModule} from "@angular/router/testing";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs/Observable";

describe('Component: pager', ()=>{
  let fixture: ComponentFixture<PagerComponent>;
  let component: PagerComponent;

  beforeEach(()=> TestBed.configureTestingModule({
    declarations: [PagerComponent],
    imports: [RouterTestingModule],
    providers: [
      {
        provide: ActivatedRoute,
        useValue: {
          params: Observable.of({page: 22})
        }
      }
    ]
  }));

  beforeEach(()=>{
    fixture = TestBed.createComponent(PagerComponent);
    component=fixture.componentInstance;
  });

  it('should set lower range',async(()=>{
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.low).toBe(21);
    });
  }));

  it('should set higer range',async(()=>{
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.high).toBe(25);
    });
  }));

  it('should set numbers array',async(()=>{
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.numbers).toEqual([21,22,23,24,25]);
    });
  }));

});
