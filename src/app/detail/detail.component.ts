import {Component} from "@angular/core";

@Component({
  selector: 'app-detail',
  template: `
    <div class="details">
      <app-navbar></app-navbar>
      <poke-detail></poke-detail>
    </div>
  `
})
export class DetailComponent{}
