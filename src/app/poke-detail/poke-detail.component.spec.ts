import {DebugElement} from "@angular/core";
import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {PokemonService} from "../pokemon/pokemon.service";
import {ActivatedRoute} from "@angular/router";
import {HttpModule} from "@angular/http";
import {PokeDetailComponent} from "./poke-detail.component";
import {Pokemon} from "../pokemon/pokemon.model";
import {By} from "@angular/platform-browser";

describe('Component: PokeDetail',()=>{
  let fixture: ComponentFixture<PokeDetailComponent>;
  let de: DebugElement;
  let component: PokeDetailComponent;
  let service: PokemonService;

  beforeEach(()=>TestBed.configureTestingModule({
    imports: [HttpModule],
    providers: [PokemonService, {provide: ActivatedRoute, useValue: {snapshot:{params:[{page:'1'}]}}}],
    declarations: [PokeDetailComponent]
  }));

  beforeEach(()=>{
    fixture = TestBed.createComponent(PokeDetailComponent);
    service = fixture.debugElement.injector.get(PokemonService);
    component=fixture.componentInstance;
    de=fixture.debugElement;
    component.router.snapshot.params={name:'bulbasaur'};
  });

  it('should load the pokemon using the service',async(()=>{
    spyOn(service,'getByName');
    spyOn(component,'loadData');
    fixture.detectChanges();
    expect(service.getByName).toHaveBeenCalled();
    component.loadData=(pokemon: Pokemon)=> {
      component.pokemon = pokemon;
      expect(component.pokemon).toBeDefined();
      expect(component.pokemon.height).toBe(10908);
    }
  }));

  it('should render the pokemon into the view',()=>{
    component.pokemon = new Pokemon('test','1','this is a test',10,'none',10,'none','none','none',10,10,10,10,10);
    fixture.detectChanges();
    expect(component.pokemon).toBeDefined();
    expect(de.query(By.css(".text-capitalize")).nativeElement.innerText).toBe('test');

  });
});
