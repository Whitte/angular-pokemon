import {Injectable} from '@angular/core';
import {Pokemon} from './pokemon.model';
import {Http, Response} from "@angular/http";
import { Observable } from "rxjs";
import 'rxjs/Rx';

@Injectable()
export class PokemonService{
  pokemons: Pokemon[][];
  private http: Http;

  constructor(http: Http){
    this.http = http;
    this.pokemons= [];
  }

  getPokemonsByPage(page: number, quantity: number){
    if(this.pokemons[page]==null){
      return this.http.get('http://pokeapi.co/api/v2/pokemon/?limit='+quantity+'&offset='+((page-1)*quantity))
        .map((response:Response) => {
          const pokemons = response.json().results;
          let transPokemons: Pokemon[] = [];
          for(let pokemon of pokemons){
            let id=pokemon.url.replace('http://pokeapi.co/api/v2/pokemon/','');
            id=parseInt(id.replace('/',''));
            transPokemons.push(new Pokemon(pokemon.name,id));
          }
          this.pokemons[page]=transPokemons;
          return transPokemons;
        })
        .catch((error:Response) => Observable.throw(error.json()));
    }
    else{
      return Observable.of(this.pokemons[page]);
    }
  }

  findByName(name: string): Pokemon{
    for(let pokemons of this.pokemons){
      let pokemon = pokemons.find((pokemon) => {
        return pokemon.name === name;
      });
      if(pokemon){ return pokemon}
      else{ return null;}
    }
    // let pokemon = this.pokemons.find((pokemon) => {
    //   return pokemon.name === name;
    // });
    // if(pokemon){ return pokemon}
    // else{ return null;}
  }

  getByName(name: string, succeed: (pokemon: Pokemon)=>any,fail?: (error: string)=>any){
    let pokemon;
    // pokemon = this.findByName(name);
    if(this.pokemons != null){
      pokemon = this.findByName(name);
      if(pokemon==null){
        pokemon = new Pokemon(name);
      }
    }
    else{
      pokemon = new Pokemon(name);
    }
    if(pokemon.description==null){
      this.getBasicData(pokemon).subscribe(()=>{
      this.getStats(pokemon).subscribe(
        succeed(pokemon),fail)
      },fail);
    }else{
      succeed(pokemon);
    }
  }

  getBasicData(pokemon: Pokemon){
    return this.http.get('http://pokeapi.co/api/v2/pokemon-species/'+pokemon.name)
      .map((response:Response) => {


        pokemon.image = response.json().id;
        pokemon.description = response.json().flavor_text_entries[3].flavor_text;
        pokemon.category = response.json().genera[2].genus;
        let stats = response.json().stats;
        if(stats){
          pokemon.speed = stats[0].base_stat;
          pokemon.s_defense = stats[1].base_stat;
          pokemon.s_attack = stats[2].base_stat;
          pokemon.defense = stats[3].base_stat;
          pokemon.attack = stats[4].base_stat;
        }

      })
      .catch((error:Response) => Observable.throw(error.json().detail));
  }

  getStats(pokemon: Pokemon){
    return this.http.get('http://pokeapi.co/api/v2/pokemon/'+pokemon.name)
      .map((response:Response) => {


        pokemon.height = response.json().height;
        pokemon.weight = response.json().weight;
        let stats = response.json().stats;

        pokemon.speed = stats[0].base_stat;
        pokemon.s_defense = stats[1].base_stat;
        pokemon.s_attack = stats[2].base_stat;
        pokemon.defense = stats[3].base_stat;
        pokemon.attack = stats[4].base_stat;
        return pokemon;
      })
      .catch((error:Response) => Observable.throw(error.json().detail));
  }

}
